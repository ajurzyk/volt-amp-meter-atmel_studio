//-------------------------------------------------------------------
#ifndef __seven_segment_main_H__
#define __seven_segment_main_H__
//-------------------------------------------------------------------

unsigned int analogReadAverage(uint8_t pin, short numReadings);
unsigned int readVoltage(uint8_t pin);

//-------------------------------------------------------------------
#endif
//-------------------------------------------------------------------
