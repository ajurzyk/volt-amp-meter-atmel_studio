/** digits to segments config

	 a
	---
  f|   |b
	-g-
  e|   |c
	---
	 d

* 0 - abcdef_
* 1 - _bc____
* 2 - abd_e_g
* 3 - abcd__g
* 4 - _bc__fg
* 5 - a_cd_fg
* 6 - a_cdefg
* 7 - abc____
* 8 - abcdefg
* 9 - abcd_fg
* A - abc_efg
* B - __cdefg
* C - a__def_
* D - _bcde_g
* E - a__defg
* F - a___efg
*
* -- other chars --
* H - _bc_efg
* L - ___def_
* O - __cde_g
* P - ab__efg
* U - _bcdef_
**/

/**
0000 0
0001 1
0010 2
0011 3
0100 4
0101 5
0110 6
0111 7
1000 8
1001 9
1010 A
1011 B
1100 C
1101 D
1110 E
1111 F
*/

#include <Arduino.h>
#include "display.h"

static const uint8_t LED_SEGMENTS[] = {
	// DP
	// |GFEDCBA
	0xC0, //B11000000 -> 0
	0xF9, //B11111001 -> 1
	0xA4, //B10100100 -> 2
	0xB0, //B10110000 -> 3
	0x99, //B10011001 -> 4
	0x92, //B10010010 -> 5
	0x82, //B10000010 -> 6
	0xF8, //B11111000 -> 7
	0x80, //B10000000 -> 8
	0x90, //B10010000 -> 9
	// special characters
	0xA3, //B10100011 -> o
	0xC7, //B11000111 -> L
	0xFF  //B11111111 -> empty (all segments off)
};
 

void printNumber(uint16_t number, const uint8_t *display) {
	uint8_t decimal_point_position = INITIAL_DECIMAL_POINT_POSITION;
	
	if (number == 9999) {
		// over range symbol
		uint8_t ol[3] = {12, 0, 11};
		return show_on_display(&decimal_point_position, ol, display);
	}

	// cut off least significant digit, our target resolution is 10mV
	number = number / 10;

	const uint8_t number_of_digits = count(number);
	
	switch (number_of_digits) {
		case 1:
		case 2:
		case 3:
			decimal_point_position = 0;
			break;
		case 4:
			number = number / 10;
			decimal_point_position = 1;
			break;
		default:
			number = number / 100;
			decimal_point_position = INITIAL_DECIMAL_POINT_POSITION;
	}
	
	uint8_t numbers[NUMBER_OF_DISPLAY_DIGITS];
	
	for (short i = NUMBER_OF_DISPLAY_DIGITS - 1; i >= 0; i--)  {
		numbers[i] = number % 10;
		number = number / 10;
	}
	
	show_on_display(&decimal_point_position, numbers, display);
}

void show_on_display(const uint8_t *decimal_point_position, const uint8_t *numbers, const uint8_t *display) {
	for (short i = NUMBER_OF_DISPLAY_DIGITS - 1; i >= 0; i--) {
		digitalWrite(display[i], LOW);
		if (i == *decimal_point_position) {
			PORTD = LED_SEGMENTS[numbers[i]] & ~(1<<7);
			} else {
			PORTD = LED_SEGMENTS[numbers[i]];
		}

		delay(1);
		//        PORTD = 0xFF;
		digitalWrite(display[i], HIGH);
	}
}

uint8_t count(uint16_t i) {
	uint8_t number_of_digits = 0;

	do {
		i = i / 10;
		number_of_digits++;
	} while (i > 0);

	return number_of_digits;
}
