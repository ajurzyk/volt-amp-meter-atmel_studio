#include <Arduino.h>
#include "display.h"
#include "volt_amp_meter.h"


// Refresh interval (in milliseconds) between ADC reading
#define REFRESH_INTERVAL 600

// Size of reading sample per input
#define NUMBER_OF_READINGS 8

// AREF voltage in millivolts - best values are 2^n like 1024, 2048, 4096... to avoid float division
#define AREF_VOLTAGE 4096

// input voltage divider ratio if used; if not, set to 1
#define INPUT_VOLTAGE_DIVIDER_RATIO 1

static const uint8_t voltage_coef = AREF_VOLTAGE/1024 * INPUT_VOLTAGE_DIVIDER_RATIO;

// Arduino pin names
static const uint8_t DISP_1[NUMBER_OF_DISPLAY_DIGITS] = {10, 9, 8};
static const uint8_t DISP_2[NUMBER_OF_DISPLAY_DIGITS] = {13,12,11};
static const uint8_t ADC_INPUTS[] = {A0, A1, A2, A3};

// user selected values from ADC_INPUTS, but we need some defaults
uint8_t selectedInputs[] = {A0, A1};

// holds readings from selected inputs
uint16_t adc0 = 0;
uint16_t adc1 = 0;

// timestamps to measure time between readings
unsigned long previousReadingTime = 0;
unsigned long currentTime;

void setup() {
	analogReference(EXTERNAL);

	// controls
	//pinMode(A4, OUTPUT);
	//pinMode(A5, OUTPUT);
	//
	//digitalWrite(A4, HIGH);
	//digitalWrite(A5, HIGH);
	
	// set six lowest pins (0-5) of port B to output mode
	DDRB = 0x3F;

	// whole PORTD is used to drive LED displays, so set it to output mode
	DDRD = 0xFF;
	
	// (switch off all digits))
	PORTB = 0x3F;
	PORTD = 0xFF;
}


void loop() {
	currentTime = millis();

	if (currentTime - previousReadingTime > REFRESH_INTERVAL) {
		previousReadingTime = currentTime;
		adc0 = readVoltage(selectedInputs[0]);
		adc1 = readVoltage(selectedInputs[1]);
	}
	
	//TODO: read keys here and show selected modes 

	printNumber(adc0, DISP_1);
	printNumber(adc1, DISP_2);
}

uint16_t readVoltage(uint8_t pin) {
	int adc_value = analogReadAverage(pin, NUMBER_OF_READINGS);
	
	if (adc_value == 1023) {
		// max ADC value reached, return something big to indicate that
		return 9999;
	}
	
	return adc_value * voltage_coef;
}


uint16_t analogReadAverage(uint8_t pin, short numReadings) {
	uint32_t total = 0;

	for (short i = 0; i < numReadings; i++) {
		delayMicroseconds(20);
		total = total + analogRead(pin);
	}

	return total / numReadings;
}
