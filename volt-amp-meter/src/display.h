#ifndef DISPLAY_H_
#define DISPLAY_H_

#define NUMBER_OF_DISPLAY_DIGITS 3

// Initially decimal point is not visible, so it can be equal to NUMBER_OF_DISPLAY_DIGITS as digit positions are counted from 0
#define INITIAL_DECIMAL_POINT_POSITION NUMBER_OF_DISPLAY_DIGITS

void show_on_display(const uint8_t *decimal_point_position, const uint8_t *numbers, const uint8_t *display);
void printNumber(uint16_t number, const uint8_t *display);
uint8_t count(uint16_t i);

#endif /* DISPLAY_H_ */